## Projext structures

- src (source)
  -- config -> sequelize config
  -- migrations ->
  -- models -> table's models
  -- seeders -> data dummy creation
  -- app.js -> app's middleware json
  --

## model

npx sequelize-cli model:generate --name user_game --attributes usernameId:string,password:string

## create customer model

```
npx sequelize-cli model:generate --name user_game_biodata --attributes fullName:string,nationality:string,age:integer,usernameId:string
```

## create order model

```
npx sequelize-cli model:generate --name user_game_history --attributes playedAt:date,usernameID:string
```

## migration

```
npx sequelize-cli  db:create
```

```
npx sequelize-cli db:migrate
```

## seeding

```
npx sequelize-cli seed:generate --name initial-sale

```

npx sequelize-cli db:seed:all

```
npx sequelize-cli db:seed:undo
```
