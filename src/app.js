const express = require("express");
const path = require("path");

const webRoute = require("./web-route");
const route = require("./route");

const pageController = require("./controllers/page.controller");

const server = (app) => {
  app.set("view engine", "ejs");
  app.set('views', path.join(__dirname, 'views'));

  app.get("/", pageController.register);
  app.post("/", pageController.signup);
  app.get("/biodata", pageController.biodata);
  app.get("/history", pageController.history);

  app.use(express.static("public"));
  
  app.use(express.json());
  app.use(express.urlencoded({ extended: true }));

  // app.use(route);
  app.use(webRoute);
  app.use("/api/v1", route);

  return app;
}


module.exports = server;